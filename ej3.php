

<?php echo '
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta name="description" content="Script PHP">
		<link rel="stylesheet" href="ejercicio3.css">
		<title>Mi Tercer Script PHP</title>
	</head>
	<body>
		<table class="products-details">
		  <tr>
			<td colspan=3 class="yellow">Productos</td>
		  </tr>
		  <tr>
			<td class="gray">Nombre</td>
			<td class="gray">Cantidad</td>
			<td class="gray">Precio (Gs)</td>
		  </tr>
		  <tr>
			<td>Coca Cola</td>
			<td>100</td>
			<td>4.500</td>
		  </tr>
		  <tr>
			<td>Pepsi</td>
			<td>30</td>
			<td>4.800</td>
		  </tr>
		  <tr>
			<td>Sprite</td>
			<td>20</td>
			<td>4.500</td>
		  </tr>
		  <tr>
			<td>Guaraná</td>
			<td>200</td>
			<td>4.500</td>
		  </tr>
		  <tr>
			<td>SevenUp</td>
			<td>24</td>
			<td>4.800</td>
		  </tr>
		  <tr>
			<td>Mirinda Naranja</td>
			<td>56</td>
			<td>4.800</td>
		  </tr>
		  <tr>
			<td>Mirinda Guaraná</td>
			<td>89</td>
			<td>4.800</td>
		  </tr>
		  <tr>
			<td>Fanta Naranja</td>
			<td>10</td>
			<td>4.500</td>
		  </tr>
		  <tr>
			<td>Fanta Piña</td>
			<td>2</td>
			<td>4.500</td>
		  </tr>
		</table> 
	</body>
</html>
';?>
